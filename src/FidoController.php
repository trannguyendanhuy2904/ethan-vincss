<?php

namespace Ethan\Vincss;


use App\TM;
use App\Role;
use App\User;
use App\UserGroup;
use App\UserSession;
use Tymon\JWTAuth\JWTAuth;
use Illuminate\Http\Request;
use Ethan\Vincss\Supports\Message;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Tymon\JWTAuth\Exceptions\JWTException;

/**
 * 
 * Class FidoController
 *
 * This class is responsible for handling Fido related operations.
 * It contains properties and methods related to Fido authentication and authorization.
 */


class FidoController
{
    /**
     * @var JWTAuth $jwt The JWTAuth instance.
     */
    protected $jwt;

    /**
     * @var int $storeId
     */
    protected $storeId;

    /**
     * @var int $companyId
     */
    protected $companyId;

    /**
     * @var string $user_role
     */
    protected $user_role;


    /**
     * Fido2Controller constructor.
     * @param JWTAuth $jwt The JWTAuth instance.
     */

    /**
     * This class contains static properties related to 
     * 
     * 
     * user types, roles, and secret keys.
     * These properties are used for authentication and authorization purposes.
     */

    protected static $_user_type_user;
    protected static $_user_expired_day;
    protected static $_user_role_guest_id;
    protected static $_user_role_guest;

    public function __construct(JWTAuth $jwt)
    {
        $this->jwt = $jwt;
        self::$_user_type_user        = "USER";
        self::$_user_role_guest = "GUEST";
        self::$_user_expired_day      = 365;

        $this->user_role = "ADMIN";
        $this->storeId   = TM::getMTS()->store_id;
        $this->companyId = TM::getMTS()->company_id;
    }

    /**
     * Handles FIDO authentication.
     *
     * @param array $tokenInfo The token information.
     * @param Request $request The request object.
     * @return User|null The user object if found, null otherwise.
     */
    private function handleFidoAuthentication($data = null)
    {

        // Kiểm tra dữ liệu được truyền vào là token hay info
        if ($data !== null && is_array($data)) {
            // Nếu là token
            if (isset($data['access_token'])) {
                $userInfo = $this->getInfo($data['access_token']);
                $userInfo = json_decode($userInfo, true);

                $username = $userInfo['username'];
                $displayName = $userInfo['displayName'] ?? null;
                $phone = $userInfo['phone'] ?? null;
                $email = $userInfo['email'] ?? null;
            

                $this->companyId = $userInfo['company_id'] ?? $this->companyId;
                $this->user_role = $userInfo['code'] ?? $this->user_role;
                $this->storeId = $userInfo['store_id'] ?? $this->storeId;

            } else {
                // Nếu là info
                $username = $data['username'];
                $displayName = $data['displayName'] ?? null;
                $phone = $data['phone'];
                $email = $data['email'] ?? null;

                $this->companyId = $data['company_id'] ?? $this->companyId;
                $this->user_role = $data['code'] ?? $this->user_role;
                $this->storeId = $data['store_id'] ?? $this->storeId;
                $this->user_role = $data['code'] ?? $this->user_role;
            }
        }

        // Tìm người dùng trong cơ sở dữ liệu
        $user = User::where('code', $username)
            ->where('store_id', $this->storeId)
            ->where('company_id', $this->companyId)
            ->where('deleted', 0)
            ->first();

        // Nếu không tìm thấy người dùng, thực hiện tạo mới
        if (!$user) {
            $role = Role::where('code', $this->user_role)->first();

            // Lấy nhóm người dùng mặc định nếu không tồn tại
            $userGroup = UserGroup::where('code', 'guest')->first();

            if (empty($userGroup)) {
                // Xử lý khi không tìm thấy nhóm người dùng
                return response()->make('', 302, ['Location' => env('APP_URL') . '?status=failure']);
            }

            try {
                DB::beginTransaction();

                $user = new User();
                $now  = date("Y-m-d H:i:s", time());
                $user->phone = $phone;
                $user->password = password_hash('12345678', PASSWORD_BCRYPT);
                $user->email = $email;
                $user->code = $username;
                $user->name = $displayName ?? strtoupper($username);
                $user->type = 'user';
                $user->register_at = $now;
                $user->is_active = 0;
                $user->role_id = $role->id;
                $user->role_code = $role->code;
                $user->role_name = $role->name;
                $user->created_by = 'system';
                $user->created_at = $now;
                $user->store_id = $this->storeId;
                $user->company_id = $this->companyId;
                $user->group_id = $userGroup->id;
                $user->group_code = $userGroup->code;
                $user->group_name = $userGroup->name;

                $user->save();
                DB::commit();
            } catch (\Exception $ex) {
                DB::rollBack();
                $errCode = $ex->getCode() == 400 ? 400 : 500;

                if (isset($data['access_token'])) {
                    return response()->make('', $errCode, ['Location' => env('APP_URL') . '?status=failure']);
                } else {
                    return $this->responseError($ex->getMessage(), $errCode);
                }
            }
        }

        return $user;
    }

    protected function responseError($msg = null, $code = 400)
    {
        $msg = $msg ? $msg : Message::get("something-went-wrong");
        return response()->json(['status' => 'error', 'error' => ['errors' => ["message" => $msg]]], $code);
    }

    /**
     * Generate the login URL for Fido2 authentication.
     *
     * @return string|null The login URL.
     */
    public function login()
    {
        if (!Auth::check()) {
            $state = bin2hex(random_bytes(5));
            $timestamp = time();

            $signature = hash_hmac('sha256', $state . $timestamp, env('VINCSS_CLIENT_SECRET'));

            $params = [
                'client_id' => env('VINCSS_CLIENT_ID'),
                'response_type' => 'code',
                'redirect_uri' => env('VINCSS_REDIRECT_URI'),
                'state' => $state . '.' . $timestamp . '.' . $signature,
            ];


            return response()->make('', 302, ['Location' =>  env('VINCSS_BASE_URL') . '/authorize?' . http_build_query($params)]);
        }
    }

    /**
     * Send a POST request to a URL with the given parameters.
     *
     * @param string $url The URL to send the request to.
     * @param array $params The parameters to include in the request.
     * @return bool|string The response from the URL.
     */
    private function postUrl($url, $params)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $headers = array();
        $headers[] = "Accept: application/json";
        $headers[] = "Authorization: xx:xxxxxxxxxxxxxxxx";
        $headers[] = "Content-Type: application/x-www-form-urlencoded";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params, '', '&'));
        $ret = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);

        if (!$err) {
            return $ret;
        }
        return false;
    }

    /**
     * Get user information using the access token.
     *
     * @param string $accessToken The access token.
     * @return mixed The user information.
     */
    private function getInfo($accessToken)
    {
        $params = [
            'access_token' => $accessToken,
        ];
        $response = $this->postUrl(env('VINCSS_BASE_URL') . '/profile', $params);

        return $response;
    }
    
    /**
     * Handle the callback after Fido2 authentication.
     *
     * @param Request $request The request object.
     * @return \Illuminate\Http\Response The response.
     */
    public function callback(Request $request)
    {

        if ($request->has('code') && $request->has('state')) {

            /**
             * Verifies the integrity of the state parameter received in the request.
             *
             * @param  \Illuminate\Http\Request  $request
             * @return \Illuminate\Http\Response
             */
            list($state, $timestamp, $signature) = explode('.', $request->state);

            if (hash_hmac('sha256', $state . $timestamp, env('VINCSS_CLIENT_SECRET')) !== $signature) {
                return response()->make('', 302, ['Location' => env('VINCSS_APP_URL') . '?status=failure']);
            }

            /**
             * Checks if the timestamp is older than 1 hour and aborts the request if true.
             *
             * @param int $timestamp The timestamp to compare against the current time.
             * @return void
             */
            if ($timestamp < time() - 3600) {
                return response()->make('', 302, ['Location' => env('VINCSS_APP_URL') . '?status=failure']);
            }

            $params = [
                'code' => $request->code,
                'client_id' => env('VINCSS_CLIENT_ID'),
                'client_secret' => env('VINCSS_CLIENT_SECRET'),
                'redirect_uri' => env('VINCSS_REDIRECT_URI'),
                'grant_type' => 'authorization_code',
            ];

            /**
             * Sends a POST request to the OAuth token endpoint and retrieves the token information.
             *
             * @param string $params The parameters to be sent in the request.
             * @return array The token information as an associative array.
             */
            $response = $this->postUrl(env('VINCSS_BASE_URL') . '/token', $params);
            $tokenInfo = json_decode($response, true);

            /**
             * This code block handles the authentication process using FIDO (Fast Identity Online) protocol.
             * It verifies the access token received from the client and retrieves user information.
             * Then, it authenticates the user using the FidoController's handleFidoAuthentication method.
             * If the authentication is successful, it creates a JWT (JSON Web Token) for the user.
             * The user's previous session is marked as deleted and a new session is created with the generated token.
             * Finally, it returns a response with a redirection URL containing the status and token information.
             *
             * @param array $tokenInfo The token information received from the client.
             * @param Request $request The HTTP request object.
             * @return Response The HTTP response object.
             */
            if (!empty($tokenInfo['access_token'])) {


                $user = $this->handleFidoAuthentication($tokenInfo);

                // verify the credentials and create a token for the user
                try {
                    if (!$token = $this->jwt->fromUser($user)) {
                        return response()->make('', 302, ['Location' => env('VINCSS_APP_URL') . '?status=failure']);
                    }
                } catch (JWTException $ex) {
                    // something went wrong
                    return response()->make('', 302, ['Location' => env('VINCSS_APP_URL') . '?status=failure']);
                }

                $time = time();
                UserSession::where('user_id', $user->id)->update([
                    'deleted' => 1,
                    'updated_at' => date("Y-m-d H:i:s", $time),
                    'updated_by' => $user->id,
                ]);

                UserSession::insert([
                    'user_id' => $user->id,
                    'token' => $token,
                    'device_token' => $request->get('device_token', null),
                    'login_at' => date("Y-m-d H:i:s", $time),
                    'expired_at'   => date("Y-m-d H:i:s", ($time + self::$_user_expired_day * 24 * 60)),
                    'deleted' => 0,
                    'created_at' => date("Y-m-d H:i:s", $time),
                    'created_by' => $user->id,
                ]);

                // if no errors are encountered we can return a JWT
                return response()->make('', 302, ['Location' => env('VINCSS_APP_URL') . '/login?status=success&token=' . $token]);
            }
        }
    }

    public function fido2_preregister(Request $request)
    {
        $input = $request->all();

        if (empty($input['username'])) {
            return $this->responseError(Message::get("missing-field", Message::get("username")), 422);
        }

        $data = [
            "username" => $input['username'],
            "displayName" => $input['displayName'] ?? null,
            "authenticatorSelection" => [
                "userVerification" => "required"
            ]
        ];


        $response = Http::withHeaders([
            "APIKEY" => env("FIDO2_APIKEY"),
            'Content-Type' => 'application/json'
        ])->post(env('ORIGIN') . '/attestation/options', $data);
        if ($response->status() != 200) {
            if ($response->status() == 400) {
                return response()->json(["message" => $response->json()["message"]], $response->status());
            }
            return response()->json(["message" => $response->json()["message"]], $response->status());
        }
        $options = $response->json();
        $cookieString = $response->getHeader('set-cookie')[0];
        return response()->json(array_merge($options, ['cc' => $cookieString]));
    }

    public function fido2_register(Request $request)
    {
        $input = $request->all();
        $response = Http::withHeaders([
            "APIKEY" => env("FIDO2_APIKEY"),
            "Cookie" => $input['cc'],
        ])->post(env('ORIGIN') . '/attestation/result', $input);

        if ($response->status() != 200) {
            if ($response->status() == 400) {
                return response()->json(["message" => $response->json()["message"]], $response->status());
            }
            return response()->json(["message" => "Internal server error"], $response->status());
        }

        return response()->json(["message" => "Register successfully"], $response->status());
    }


    public function fido2_prelogin(Request $request)
    {
        $input = $request->all();

        if (empty($input['username'])) {
            return $this->responseError(Message::get("missing-field", Message::get("username")), 422);
        };

        $response = Http::withHeaders([
            "APIKEY" => env("FIDO2_APIKEY"),
            'Content-Type' => 'application/json'
        ])->post(env('ORIGIN') . '/assertion/options', $input);

        if ($response->status() != 200) {
            if ($response->status() == 400) {
                return response()->json(["message" => $response->json()["message"]], $response->status());
            }
            return response()->json(["message" => "Internal server error"], $response->status());
        }

        $options = $response->json();
        $cookieString = $response->getHeader('set-cookie')[0];
        return response()->json(array_merge($options, ['cc' => $cookieString]));
    }

    public function fido2_login(Request $request)
    {

        $input = $request->all();

        $response = Http::withHeaders([
            "APIKEY" => env("FIDO2_APIKEY"),
            "Cookie" => $input['cc'],
            'Content-Type' => 'application/json'
        ])->post(env('ORIGIN') . '/assertion/result', $input);


        if ($response->status() != 200) {
            if ($response->status() == 400) {
                return response()->json(["message" => $response->json()["message"]], $response->status());
            }
            return response()->json(["message" => "Internal server error"], $response->status());
        }


        $info = $response->json();


        if ($info) {
            $user = $this->handleFidoAuthentication($info);

            try {
                if (!$token = $this->jwt->fromUser($user)) {
                    return response()->json(['error' => 'invalid_credentials'], 401);
                }
            } catch (JWTException $ex) {
                // something went wrong
                return response()->json(['error' => 'could_not_create_token'], 500);
            }

            $time = time();
            UserSession::where('user_id', $user->id)->update([
                'deleted' => 1,
                'updated_at' => date("Y-m-d H:i:s", $time),
                'updated_by' => $user->id,
            ]);

            UserSession::insert([
                'user_id' => $user->id,
                'token' => $token,
                'device_token' => $request->get('device_token', null),
                'login_at' => date("Y-m-d H:i:s", $time),
                'expired_at'   => date("Y-m-d H:i:s", ($time + self::$_user_expired_day * 24 * 60)),
                'deleted' => 0,
                'created_at' => date("Y-m-d H:i:s", $time),
                'created_by' => $user->id,
            ]);

            return response()->json([
                'token'          => $token,
                'user_id'        => $user->id,
                'user_type'      => $user->type,
                'role_id'        => $user->role_id,
                'role_code'      => $user->role->code,
                'role_name'      => $user->role->name,
                'account_status' => $user->account_status,
                'group_code'     => $user->group_code,
            ]);
        }
    }
}
