<?php

namespace Ethan\Vincss;
use Tymon\JWTAuth\JWTAuth;
use Illuminate\Support\ServiceProvider;

/**
 * The VincssServiceProvider class is responsible for registering and bootstrapping the Vincss package.
 */
class VincssServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('vincss', function ($app) {
            return new \Ethan\Vincss\FidoController($app->make(JWTAuth::class));
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->router->group([], function ($router) {
            require __DIR__.'/routes.php';
        });
    }
}